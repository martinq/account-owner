package com.qapil.vuesample.accountowner.web.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GreetingsControllerTest {

    @Autowired
    private MockMvc mvc;

    @Value("${app.baseApiPath}")
    private String baseApiPath;

    @Test
    public void getHello() throws Exception {
        // mvc.perform(MockMvcRequestBuilders.get(baseApiPath + "/hello").accept(MediaType.APPLICATION_JSON))
        //         .andExpect(MockMvcResultMatchers.status().isOk())
        //         .andExpect(MockMvcResultMatchers.content().string(Matchers.equalTo("Greetings from Spring Boot!")))
        // ;
    }
}
