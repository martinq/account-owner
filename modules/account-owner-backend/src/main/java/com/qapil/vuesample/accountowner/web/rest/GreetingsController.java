package com.qapil.vuesample.accountowner.web.rest;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("${app.baseApiPath}")
public class GreetingsController {

    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }

    @RequestMapping("/hello")
    public String hello() {
        return "Greetings from Spring Boot!";
    }
}
