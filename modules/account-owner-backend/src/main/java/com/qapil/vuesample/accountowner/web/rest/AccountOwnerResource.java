package com.qapil.vuesample.accountowner.web.rest;

import com.qapil.vuesample.accountowner.web.rest.vm.AccountVM;
import com.qapil.vuesample.accountowner.web.rest.vm.OwnerVM;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("${app.baseApiPath}/owner")
public class AccountOwnerResource {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final Map<Integer, OwnerVM> owners = new TreeMap<>();
    private final AtomicInteger nextId = new AtomicInteger(1);

    public AccountOwnerResource() {
        OwnerVM owner1 = new OwnerVM(nextId.getAndIncrement(), "one", "address1", LocalDate.of(2001, 1, 1));
        owner1.setAccounts(new AccountVM[] {
                new AccountVM(1, "type1", parseDate("2011-01-01 12:00:00")),
                new AccountVM(2, "type2", parseDate("2011-01-02 13:00:00"))});
        OwnerVM owner2 = new OwnerVM(nextId.getAndIncrement(), "two", "address2", LocalDate.of(2002, 2, 2));
        OwnerVM owner3 = new OwnerVM(nextId.getAndIncrement(), "three", "address3", LocalDate.of(2003, 3, 3));

        owners.put(owner1.getId(), owner1);
        owners.put(owner2.getId(), owner2);
        owners.put(owner3.getId(), owner3);
    }

    private Date parseDate(String s) {
        return dateFormat.parse(s, new ParsePosition(0));
    }

    @GetMapping()
    public Collection<OwnerVM> getAllOwners() {
        return owners.values();
    }

    @GetMapping("/{ownerId}")
    public OwnerVM getById(@PathVariable int ownerId) {
        OwnerVM owner = owners.get(ownerId);
        if (owner == null) {
            throw new NotFoundException("Owner with id " + ownerId + " not found.");
        }
        return owner;
    }

    @PostMapping()
    public ResponseEntity createOwner(@RequestBody OwnerVM owner, HttpServletRequest request) {
        owner.setId(nextId.getAndIncrement());
        owners.put(owner.getId(), owner);
        return ResponseEntity.created(URI.create(request.getRequestURI() + "/" + owner.getId())).build();
    }

    @PutMapping("/{ownerId}")
    public ResponseEntity updateOwner(@PathVariable int ownerId, @RequestBody OwnerVM owner, HttpServletRequest request) {
        OwnerVM existingOwner = owners.get(ownerId);
        if (existingOwner == null) {
            throw new NotFoundException("Cannot update owner. Owner with id " + ownerId + " not found.");
        }
        existingOwner.setName(owner.getName());
        existingOwner.setDateOfBirth(owner.getDateOfBirth());
        existingOwner.setAddress(owner.getAddress());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{ownerId}/account")
    public OwnerVM getAccount(@PathVariable int ownerId) {
        OwnerVM owner = owners.get(ownerId);
        if (owner == null) {
            throw new NotFoundException("Owner with id " + ownerId + " not found.");
        }
        return owner;
    }

    @DeleteMapping("/{ownerId}")
    public ResponseEntity deleteOwner(@PathVariable int ownerId) {
        OwnerVM owner = owners.get(ownerId);
        if (owner == null) {
            throw new NotFoundException("Cannot delete owner. Owner with id " + ownerId + " not found.");
        } else if (owner.getAccounts() != null && owner.getAccounts().length > 0) {
            throw new BadRequestException("Cannot delete owner. It has related accounts. Delete those accounts first.");
        }
        owners.remove(ownerId);
        return ResponseEntity.noContent().build();
    }
}
