package com.qapil.vuesample.accountowner.web.rest.vm;

import java.time.LocalDate;
import java.util.Arrays;

public class OwnerVM {

    private Integer id;
    private String name;
    private String address;
    private LocalDate dateOfBirth;
    private AccountVM[] accounts = new AccountVM[0];

    public OwnerVM() {
    }

    public OwnerVM(Integer id, String name, String address, LocalDate dateOfBirth) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public AccountVM[] getAccounts() {
        return accounts;
    }

    public void setAccounts(AccountVM[] accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "OwnerVM{" + "id=" + id + ", name='" + name + '\'' + ", address='" + address + '\'' + ", dateOfBirth=" + dateOfBirth
                + ", accounts=" + Arrays.toString(accounts) + '}';
    }
}
