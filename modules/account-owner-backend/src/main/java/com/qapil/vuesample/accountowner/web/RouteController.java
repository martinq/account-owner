package com.qapil.vuesample.accountowner.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RouteController {

    @RequestMapping("/login")
    public String login() {
        // return "forward:/";
        return "login.html";
    }

    /**
     * Forward all non asset requests to index page ('/') that will handle the path.
     * API requests are handled by different controller before this one.
     */
    // @RequestMapping("/**/{path:[^\\.]*}")
    // public String index() {
    //     return "forward:/";
    // }
}
