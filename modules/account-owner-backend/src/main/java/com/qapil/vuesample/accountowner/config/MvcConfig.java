package com.qapil.vuesample.accountowner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Value("${app.baseApiPath}")
    public String baseApiPath;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // registry.addResourceHandler("/", "/**", "/login**")
        //         .setCachePeriod(0)
        //         .addResourceLocations("classpath:/static/");
        // registry.addResourceHandler("/**/*.css", "/**/*.js", "/**/*.js.map", "/**/*.png", "/**/*.ttf")
        //         .setCachePeriod(0)
        //         .addResourceLocations("classpath:/static/");
        //
        // registry.addResourceHandler("/", "/**")
        //
        //        .setCachePeriod(0)
        //        .addResourceLocations("classpath:/static/index.html")
        //        .resourceChain(true)
        //        .addResolver(new PathResourceResolver() {
        //            @Override
        //            protected Resource getResource(String resourcePath, Resource location) throws IOException {
        //                if (resourcePath.startsWith(baseApiPath) || resourcePath.startsWith(baseApiPath.substring(1))) {
        //                    return null;
        //                }
        //                return location.exists() && location.isReadable() ? location : null;
        //            }
        //        });
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        String[] methods = new String[] {
                HttpMethod.GET.name(),
                HttpMethod.HEAD.name(),
                HttpMethod.POST.name(),
                HttpMethod.PUT.name(),
                HttpMethod.DELETE.name()
        };
        registry.addMapping(baseApiPath + "/**").allowedOrigins("http://localhost:8000").allowedMethods(methods);
    }
}
