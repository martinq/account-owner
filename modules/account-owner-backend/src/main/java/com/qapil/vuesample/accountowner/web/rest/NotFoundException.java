package com.qapil.vuesample.accountowner.web.rest;

import com.qapil.vuesample.accountowner.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends ApplicationException {

    public NotFoundException(String message) {
        super(message);
    }
}
