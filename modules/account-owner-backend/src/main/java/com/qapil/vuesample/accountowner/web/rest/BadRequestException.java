package com.qapil.vuesample.accountowner.web.rest;

import com.qapil.vuesample.accountowner.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends ApplicationException {

    public BadRequestException(String message) {
        super(message);
    }
}
