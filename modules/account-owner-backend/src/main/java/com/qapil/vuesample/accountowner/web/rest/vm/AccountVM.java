package com.qapil.vuesample.accountowner.web.rest.vm;

import java.util.Date;

public class AccountVM {
    private Integer id;
    private String accountType;
    private Date dateCreated;

    public AccountVM() {
    }

    public AccountVM(Integer id, String accountType, Date dateCreated) {
        this.id = id;
        this.accountType = accountType;
        this.dateCreated = dateCreated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        return "AccountVM{" + "id=" + id + ", accountType='" + accountType + '\'' + ", dateCreated=" + dateCreated + '}';
    }
}
