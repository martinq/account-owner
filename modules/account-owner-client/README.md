# account-owner-client

## Create project
```
vue create account-owner-client
vue add router
vua add vue-cli-plugin-bootstrap-vue
vue add vue-cli-plugin-i18n
npm install axios
npm install vue-cookies
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
