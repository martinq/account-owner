import Vue from 'vue'
import Router from 'vue-router'
import NotFound from '@/error-pages/NotFound.vue'
// import Home from './views/Home.vue'
import Home from '@/components/Home.vue'
import OwnerList from '@/components/owner/OwnerList'
import OwnerDetails from "./components/owner/OwnerDetails";
import OwnerCreate from "./components/owner/OwnerCreate";
import OwnerUpdate from "./components/owner/OwnerUpdate";
// import {i18n} from '@/plugins/i18n'
import About from '@/views/About'

Vue.use(Router)


let router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        // {
        //     path: '/login',
        //     name: 'Login',
        //     component: Login,
        //     meta: {
        //         guest: true
        //     }
        // },
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/owner/list',
            name: 'OwnerList',
            component: OwnerList,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/owner/:id',
            name: 'OwnerDetails',
            component: OwnerDetails,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/owner/create',
            name: 'OwnerCreate',
            component: OwnerCreate,
            meta: {
                requiresAuth: true,
                isAdmin: true
            }
        },
        {
            path: '/owner/update/:id',
            name: 'OwnerUpdate',
            component: OwnerUpdate,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '*',
            name: 'NotFound',
            component: NotFound
        }
    ]
})

router.beforeEach((to, from, next) => {
    next()

    // if (to.matched.some(record => record.meta.requiresAuth)) {
    //     if (localStorage.getItem('jwt') == null) {
    //         next({
    //             path: '/login',
    //             params: { nextUrl: to.fullPath }
    //         })
    //     } else {
    //         let user = JSON.parse(localStorage.getItem('user'))
    //         if (to.matched.some(record => record.meta.isAdmin)) {
    //             if (user.isAdmin == 1) {
    //                 next()
    //             } else {
    //                 next({ name: 'OwnerList' })
    //             }
    //         } else {
    //             next()
    //         }
    //     }
    // } else if (to.matched.some(record => record.meta.guest)) {
    //     if (localStorage.getItem('jwt') == null) {
    //         next()
    //     } else {
    //         next({ name: 'OwnerList' })
    //     }
    // } else {
    //     next()
    // }
})

export default router
