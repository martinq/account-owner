import Vue from 'vue'
// import Axios from "axios";

const RESOURCE_NAME = '/owner'

export default {
  getAll() {
    return Vue.axios.get(RESOURCE_NAME)
  },
  getById(id) {
    return Vue.axios.get(`${RESOURCE_NAME}/${id}`)
  },
  create(data) {
    return Vue.axios.post(RESOURCE_NAME, data)
  },
  update(id, data) {
    return Vue.axios.put(`${RESOURCE_NAME}/${id}`, data)
  },
  delete(id) {
    return Vue.axios.delete(`${RESOURCE_NAME}/${id}`);
  },
  getAccounts(id) {
    return Vue.axios.get(`${RESOURCE_NAME}/${id}/account`);
  }
}
