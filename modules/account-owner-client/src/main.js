import Vue from 'vue'
import App from './App'
import router from './router'
import '@babel/polyfill'
import '@/plugins/bootstrap-vue'
import '@/plugins/axios'
import '@/plugins/vue-cookies'
import {i18n} from '@/plugins/i18n'
// import i18n from '@/plugins/i18n'

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
