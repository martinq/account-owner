module.exports = {
    publicPath: '/',
    assetsDir: 'assets',
    // publicPath: process.env.NODE_ENV === 'production' ? './' : '/',

    devServer: {
        port: 8000,
        // proxy: 'http://localhost:8080',
        proxy: {
            '^/api': {
                target: 'http://localhost:8080',
                ws: true,
                changeOrigin: true
            },
            '^/login': {
                target: 'http://localhost:8080',
                ws: true,
                changeOrigin: true
            }
        }

        // proxy: [{
        //     context: [
        //         '^/api',
        //         '^/login'
        //     ],
        //     target: 'http://localhost:8080',
        //     ws: true,
        //     changeOrigin: true
        // }]

        // proxy: [{
        //     context: [
        //         '/api',
        //         '/api/owner',
        //         '/login',
        //         '/login/facebook',
        //         '/logout'
        //     ],
        //     target: 'http://127.0.0.1:8080',
        //     secure: false,
        //     changeOrigin: false,
        //     headers: { host: 'localhost:8000' }
        // }]

        // contentBase: './build/www',
        // proxy: [{
        //     context: [
        //         /* jhipster-needle-add-entity-to-webpack - JHipster will add entity api paths here */
        //         '/api',
        //         '/management',
        //         '/swagger-resources',
        //         '/v2/api-docs',
        //         '/h2-console',
        //         '/auth'
        //     ],
        //     target: `http${options.tls ? 's' : ''}://127.0.0.1:8080`,
        //     secure: false,
        //     changeOrigin: options.tls,
        //     headers: { host: 'localhost:9000' }
        // }],
        // stats: options.stats,
        // watchOptions: {
        //     ignored: /node_modules/
        // }
    },

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: true
      }
    }
}
